# Online Radio
This the test task

## TABLE OF CONTENTS
- [Features](#features)
- [Installation](docs/installation.md)
    - [Manual installation](docs/installation.md#manual-installation)
    - [Vagrant installation](docs/installation.md#vagrant-installation)
- [Components documentation](docs/components.md)
- [Console commands](docs/console.md)
- [FAQ](docs/faq.md)

## FEATURES
### Admin backend
- Beautiful and open source dashboard theme for backend [AdminLTE 2](http://almsaeedstudio.com/AdminLTE)
- Settings editor. Application settings form (based on KeyStorage component)
- Users, RBAC management
- Logs viewer

### I18N
- Language switcher, built-in behavior to choose locale based on browser preferred language

### Users
- Sign in
- Sign up
- Profile editing(avatar, locale, personal data)
- RBAC with predefined `guest`, `user`, `manager` and `administrator` roles
- RBAC migrations support

### Development
- .env support
- Ready to use REST API module
- On-demand thumbnail creation [trntv/yii2-glide](https://github.com/trntv/yii2-glide)

### Other
- Useful behaviors (GlobalAccessBehavior, CacheInvalidateBehavior)
- [Aceeditor widget](https://github.com/trntv/yii2-aceeditor)
- [Datetimepicker widget](https://github.com/trntv/yii2-bootstrap-datetimepicker),
- [Imperavi Reactor Widget](https://github.com/asofter/yii2-imperavi-redactor),
- [Xhprof Debug panel](https://github.com/trntv/yii2-debug-xhprof)
- Extended IDE autocompletion
- Vagrant support
