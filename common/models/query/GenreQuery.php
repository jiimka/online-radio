<?php

namespace common\models\query;


use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\Genre]].
 *
 * @see \common\models\Genre
 */
class GenreQuery extends ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \common\models\Genre[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Genre|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
