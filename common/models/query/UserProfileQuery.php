<?php

namespace common\models\query;


use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\UserProfile]].
 *
 * @see \common\models\UserProfile
 */
class UserProfileQuery extends ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \common\models\UserProfile[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\UserProfile|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
