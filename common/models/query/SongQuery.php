<?php

namespace common\models\query;


use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\Song]].
 *
 * @see \common\models\Song
 */
class SongQuery extends ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \common\models\Song[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Song|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
