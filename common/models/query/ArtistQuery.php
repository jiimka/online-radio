<?php

namespace common\models\query;


use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\Artist]].
 *
 * @see \common\models\Artist
 */
class ArtistQuery extends ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \common\models\Artist[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Artist|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
