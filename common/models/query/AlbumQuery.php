<?php

namespace common\models\query;


use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\Album]].
 *
 * @see \common\models\Album
 */
class AlbumQuery extends ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \common\models\Album[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\Album|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
