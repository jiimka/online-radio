<?php

namespace common\models\query;


use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\SongBroadcast]].
 *
 * @see \common\models\SongBroadcast
 */
class SongBroadcastQuery extends ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \common\models\SongBroadcast[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\SongBroadcast|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
