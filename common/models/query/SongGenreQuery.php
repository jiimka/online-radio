<?php

namespace common\models\query;


use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\SongGenre]].
 *
 * @see \common\models\SongGenre
 */
class SongGenreQuery extends ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \common\models\SongGenre[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\models\SongGenre|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
