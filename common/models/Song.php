<?php

namespace common\models;


use common\models\query\SongQuery;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "song".
 *
 * @property int $id
 * @property string $title
 * @property int $artist_id
 * @property int $album_id
 * @property string $duration
 *
 * @property Album $album
 * @property Artist $artist
 * @property SongGenre[] $songGenres
 * @property Genre[] $genres
 */
class Song extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%song}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'artist_id', 'duration'], 'required'],
            [['artist_id', 'album_id'], 'integer'],
            [['duration'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['album_id'], 'exist', 'skipOnError' => true, 'targetClass' => Album::class, 'targetAttribute' => ['album_id' => 'id']],
            [['artist_id'], 'exist', 'skipOnError' => true, 'targetClass' => Artist::class, 'targetAttribute' => ['artist_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'title' => Yii::t('common', 'Title'),
            'artist_id' => Yii::t('common', 'Artist ID'),
            'album_id' => Yii::t('common', 'Album ID'),
            'duration' => Yii::t('common', 'Duration'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlbum()
    {
        return $this->hasOne(Album::class, ['id' => 'album_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArtist()
    {
        return $this->hasOne(Artist::class, ['id' => 'artist_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSongGenres()
    {
        return $this->hasMany(SongGenre::class, ['song_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getGenres()
    {
        return $this->hasMany(Genre::class, ['id' => 'genre_id'])->viaTable('song_genre', ['song_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return SongQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SongQuery(get_called_class());
    }
}
