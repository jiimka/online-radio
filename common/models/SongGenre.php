<?php

namespace common\models;


use common\models\query\SongGenreQuery;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "song_genre".
 *
 * @property int $song_id
 * @property int $genre_id
 *
 * @property Genre $genre
 * @property Song $song
 */
class SongGenre extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%song_genre}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['song_id', 'genre_id'], 'required'],
            [['song_id', 'genre_id'], 'integer'],
            [['song_id', 'genre_id'], 'unique', 'targetAttribute' => ['song_id', 'genre_id']],
            [['genre_id'], 'exist', 'skipOnError' => true, 'targetClass' => Genre::class, 'targetAttribute' => ['genre_id' => 'id']],
            [['song_id'], 'exist', 'skipOnError' => true, 'targetClass' => Song::class, 'targetAttribute' => ['song_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'song_id' => Yii::t('common', 'Song ID'),
            'genre_id' => Yii::t('common', 'Genre ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGenre()
    {
        return $this->hasOne(Genre::class, ['id' => 'genre_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSong()
    {
        return $this->hasOne(Song::class, ['id' => 'song_id']);
    }

    /**
     * {@inheritdoc}
     * @return SongGenreQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SongGenreQuery(get_called_class());
    }
}
