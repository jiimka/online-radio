<?php

namespace common\models;


use common\models\query\AlbumQuery;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "album".
 *
 * @property int $id
 * @property string $title
 * @property int $year
 * @property int $artist_id
 *
 * @property Artist $artist
 * @property Song[] $songs
 */
class Album extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%album}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'year', 'artist_id'], 'required'],
            [['year', 'artist_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['artist_id'], 'exist', 'skipOnError' => true, 'targetClass' => Artist::class, 'targetAttribute' => ['artist_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'title' => Yii::t('common', 'Title'),
            'year' => Yii::t('common', 'Year'),
            'artist_id' => Yii::t('common', 'Artist ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArtist()
    {
        return $this->hasOne(Artist::class, ['id' => 'artist_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSongs()
    {
        return $this->hasMany(Song::class, ['album_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return AlbumQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AlbumQuery(get_called_class());
    }
}
