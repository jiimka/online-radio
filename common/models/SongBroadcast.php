<?php

namespace common\models;


use common\models\query\SongBroadcastQuery;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "song_broadcast".
 *
 * @property int $id
 * @property int $song_id
 * @property string $broadcast
 * @property string $date
 *
 * @property Song $song
 */
class SongBroadcast extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%song_broadcast}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['song_id', 'broadcast', 'date'], 'required'],
            [['song_id', 'broadcast'], 'integer'],
            [['date'], 'safe'],
            [['song_id'], 'exist', 'skipOnError' => true, 'targetClass' => Song::class, 'targetAttribute' => ['song_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'song_id' => Yii::t('common', 'Song ID'),
            'broadcast' => Yii::t('common', 'Broadcast'),
            'date' => Yii::t('common', 'Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSong()
    {
        return $this->hasOne(Song::class, ['id' => 'song_id']);
    }

    /**
     * {@inheritdoc}
     * @return SongBroadcastQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SongBroadcastQuery(get_called_class());
    }
}
