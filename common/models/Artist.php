<?php

namespace common\models;


use common\models\query\ArtistQuery;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "artist".
 *
 * @property int $id
 * @property string $name
 *
 * @property Song[] $songs
 */
class Artist extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%artist}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSongs()
    {
        return $this->hasMany(Song::class, ['artist_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ArtistQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ArtistQuery(get_called_class());
    }
}
