<?php


namespace common\base;

use Swift_SmtpTransport;


class SwiftSmtpTransport extends Swift_SmtpTransport {

    /**
     * SwiftSmtpTransport constructor.
     *
     * @param string $host
     * @param int $port
     * @param null $encryption
     */
    public function __construct($host = 'localhost', $port = 25, $encryption = null)
    {
        parent::__construct($host, $port, $encryption);

        $this->setTimeout(env('SMTP_TIMEOUT') ?: 5);
    }
}
