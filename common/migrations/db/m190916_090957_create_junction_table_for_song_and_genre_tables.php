<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%song_genre}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%song}}`
 * - `{{%genre}}`
 */
class m190916_090957_create_junction_table_for_song_and_genre_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%song_genre}}', [
            'song_id' => $this->integer(),
            'genre_id' => $this->integer(),
            'PRIMARY KEY(song_id, genre_id)',
        ]);

        // creates index for column `song_id`
        $this->createIndex(
            '{{%idx-song_genre-song_id}}',
            '{{%song_genre}}',
            'song_id'
        );

        // add foreign key for table `{{%song}}`
        $this->addForeignKey(
            '{{%fk-song_genre-song_id}}',
            '{{%song_genre}}',
            'song_id',
            '{{%song}}',
            'id',
            'CASCADE'
        );

        // creates index for column `genre_id`
        $this->createIndex(
            '{{%idx-song_genre-genre_id}}',
            '{{%song_genre}}',
            'genre_id'
        );

        // add foreign key for table `{{%genre}}`
        $this->addForeignKey(
            '{{%fk-song_genre-genre_id}}',
            '{{%song_genre}}',
            'genre_id',
            '{{%genre}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%song}}`
        $this->dropForeignKey(
            '{{%fk-song_genre-song_id}}',
            '{{%song_genre}}'
        );

        // drops index for column `song_id`
        $this->dropIndex(
            '{{%idx-song_genre-song_id}}',
            '{{%song_genre}}'
        );

        // drops foreign key for table `{{%genre}}`
        $this->dropForeignKey(
            '{{%fk-song_genre-genre_id}}',
            '{{%song_genre}}'
        );

        // drops index for column `genre_id`
        $this->dropIndex(
            '{{%idx-song_genre-genre_id}}',
            '{{%song_genre}}'
        );

        $this->dropTable('{{%song_genre}}');
    }
}
