<?php

use common\models\{Album, Artist, Song};
use yii\db\Migration;

/**
 * Handles the creation of table `{{%song}}`.
 */
class m190916_063005_create_song_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Song::tableName(), [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'artist_id' => $this->integer()->notNull(),
            'album_id' => $this->integer()->null(),
            'duration' => $this->time()->notNull(),
        ], $this->db->driverName === 'mysql' ? 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB' : null);

        $this->addForeignKey('fk_song_album', Song::tableName(), 'album_id', Album::tableName(), 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_song_artist', Song::tableName(), 'artist_id', Artist::tableName(), 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_song_album', Song::tableName());
        $this->dropForeignKey('fk_song_artist', Song::tableName());

        $this->dropTable(Song::tableName());
    }
}
