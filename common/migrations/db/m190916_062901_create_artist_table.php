<?php

use common\models\Artist;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%artist}}`.
 */
class m190916_062901_create_artist_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Artist::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ], $this->db->driverName === 'mysql' ? 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB' : null);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(Artist::tableName());
    }
}
