<?php

use common\models\Genre;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%genre}}`.
 */
class m190916_062843_create_genre_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Genre::tableName(), [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
        ], $this->db->driverName === 'mysql' ? 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB' : null);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(Genre::tableName());
    }
}
