<?php

use common\models\{Song, SongBroadcast};
use yii\db\Migration;

/**
 * Handles the creation of table `{{%song_broadcast}}`.
 */
class m190916_174645_create_song_broadcast_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(SongBroadcast::tableName(), [
            'id' => $this->primaryKey(),
            'song_id' => $this->integer()->notNull(),
            'broadcast' => $this->bigInteger()->unsigned()->notNull(),
            'date' => $this->date()->notNull(),
        ], $this->db->driverName === 'mysql' ? 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB' : null);

        $this->addForeignKey(
            '{{%fk-song_broadcast-song_id}}',
            SongBroadcast::tableName(),
            'song_id',
            Song::tableName(),
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-song_broadcast-song_id}}', SongBroadcast::tableName());

        $this->dropTable(SongBroadcast::tableName());
    }
}
