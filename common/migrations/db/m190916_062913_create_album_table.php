<?php

use common\models\{Album, Artist};
use yii\db\Migration;

/**
 * Handles the creation of table `{{%album}}`.
 */
class m190916_062913_create_album_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Album::tableName(), [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'year' => $this->smallInteger()->unsigned()->notNull(),
            'artist_id' => $this->integer()->notNull(),
        ], $this->db->driverName === 'mysql' ? 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB' : null);

        $this->addForeignKey('fk_album_artist', Album::tableName(), 'artist_id', Artist::tableName(), 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_album_artist', Album::tableName());

        $this->dropTable(Album::tableName());
    }
}
