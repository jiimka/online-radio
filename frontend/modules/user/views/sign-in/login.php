<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\modules\user\models\LoginForm */

$this->title = Yii::t('frontend', 'Log in');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card mb-3 text-secondary site-sign-in site-sign-in--login mx-auto px-2 pt-2 pb-5">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <h4 class="text-info mb-4"><?= Html::encode($this->title); ?></h4>

                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'layout' => ActiveForm::LAYOUT_HORIZONTAL,
                    'fieldConfig' => [
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-4',
                            'offset' => 'offset-sm-4',
                            'wrapper' => 'col-sm-8',
                        ],
                    ],
                    'options' => [
                        'class' => 'required-fields-highlighted',
                    ],
                ]); ?>
                <?php echo $form->field($model, 'identity')->input('email') ?>
                <?php echo $form->field($model, 'password')->passwordInput() ?>
                <?php echo $form->field($model, 'rememberMe')->checkbox() ?>

                <div class="form-group row mt-5">
                    <?php echo Html::submitButton(Yii::t('frontend', 'Log in'), [
                        'class' => 'btn btn-info btn-lg btn-block w-100 rounded-0',
                        'name' => 'signup-button',
                    ]); ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
