<?php
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form \yii\bootstrap4\ActiveForm */
/* @var $model \frontend\modules\user\models\SignupForm */

$this->title = Yii::t('frontend', 'Create Account');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card mb-3 text-secondary site-sign-in site-sign-in--signup mx-auto px-2 py-2">
    <div class="card-body">
        <div class="col-lg-12">
            <h4 class="text-info mb-4"><?= Html::encode($this->title); ?></h4>

            <?php $form = ActiveForm::begin([
                'id' => 'form-signup',
                'layout' => ActiveForm::LAYOUT_HORIZONTAL,
                'fieldConfig' => [
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-4',
                        'offset' => 'offset-sm-4',
                        'wrapper' => 'col-sm-8',
                    ],
                ],
                'options' => [
                    'class' => 'required-fields-highlighted',
                ],
            ]); ?>
            <?php echo $form->field($model, 'firstName')->textInput(['maxlength' => true]); ?>
            <?php echo $form->field($model, 'lastName')->textInput(['maxlength' => true]); ?>
            <?php echo $form->field($model, 'email')->input('email', ['maxlength' => true]); ?>
            <?php echo $form->field($model, 'password')->passwordInput(['maxlength' => true]); ?>
            <?php echo $form->field($model, 'passwordConfirm')->passwordInput(['maxlength' => true]); ?>
            <?php echo $form->field($model, 'language')->dropDownList(Yii::$app->params['availableLocales']); ?>
            <div class="form-group mt-5">
                <?php echo Html::submitButton(Yii::t('frontend', 'Sign up'), ['class' => 'btn btn-info btn-lg btn-block w-100 rounded-0', 'name' => 'signup-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
