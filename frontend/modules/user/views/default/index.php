<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model common\base\MultiModel */

$this->title = Yii::t('frontend', 'My Account');
/** @var \frontend\modules\user\models\AccountForm $modelAccount */
$modelAccount = $model->getModel('account');
/** @var \common\models\UserProfile $modelProfile */
$modelProfile = $model->getModel('profile');
?>
<div class="card mb-3 text-secondary user-profile-form mx-auto px-2 py-2">
    <div class="card-body">
        <?php $form = ActiveForm::begin([
            'id' => 'form-profile',
            'layout' => ActiveForm::LAYOUT_HORIZONTAL,
            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'label' => 'col-sm-3',
                    'offset' => 'offset-sm-3',
                    'wrapper' => 'col-sm-9',
                ],
            ],
        ]); ?>
        <div class="row">
            <div class="col-12 mb-3">
                <h5 class="text-info"><?= Yii::t('frontend', 'Hello'); ?>, <?= $modelProfile->getFullName(); ?>!</h5>
            </div>

            <div class="col-md-6">
                <?php echo $form->field($modelProfile, 'firstname')->textInput(['maxlength' => true]); ?>
                <?php echo $form->field($modelProfile, 'lastname')->textInput(['maxlength' => true]); ?>
                <?php echo $form->field($modelAccount, 'email'); ?>
            </div>

            <div class="col-md-6">
                <?php echo $form->field($modelAccount, 'password')->passwordInput(['placeholder' => Yii::t('frontend', 'Leave empty if not changed')]); ?>
                <?php echo $form->field($modelProfile, 'locale')
                    ->dropDownlist(Yii::$app->params['availableLocales']); ?>
            </div>
        </div>

        <div class="form-group text-right">
            <?php echo Html::submitButton(Yii::t('frontend', 'Update'), ['class' => 'btn btn-info']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
