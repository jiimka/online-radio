<?php

namespace frontend\modules\user\models;

use cheatsheet\Time;
use common\models\{User, UserToken};
use frontend\modules\user\Module;
use Yii;
use yii\base\{Exception, Model};
use yii\helpers\Url;
use yii\web\JsExpression;

/**
 * Signup form
 */
class SignupForm extends Model
{
    /** @var string */
    public $firstName;
    /** @var string */
    public $lastName;
    /** @var string */
    public $email;
    /** @var string */
    public $password;
    /** @var string */
    public $passwordConfirm;
    /** @var string */
    public $language;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstName', 'lastName', 'language'], 'filter', 'filter' => 'trim'],
            [['firstName', 'lastName'], 'required'],
            [['firstName', 'lastName'], 'string', 'min' => 2, 'max' => 255],
            [['language'], 'string', 'min' => 2, 'max' => 5],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique',
                'targetClass' => '\common\models\User',
                'message' => Yii::t('frontend', 'This email address has already been taken.')
            ],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            [
                'passwordConfirm',
                'required',
                'when' => function ($model) {
                    return !empty($model->password);
                },
                'whenClient' => new JsExpression("function (attribute, value) {
                    return $('#signupform-password').val().length > 0;
                }")
            ],
            ['passwordConfirm', 'compare', 'compareAttribute' => 'password', 'skipOnEmpty' => false],

        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'firstName' => Yii::t('frontend', 'First Name'),
            'lastName' => Yii::t('frontend', 'Last Name'),
            'email' => Yii::t('frontend', 'E-mail'),
            'password' => Yii::t('frontend', 'Password'),
            'passwordConfirm' => Yii::t('frontend', 'Confirm Password'),
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     * @throws Exception
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->email;
            $user->email = $this->email;
            $user->status = User::STATUS_ACTIVE;
            $user->setPassword($this->password);
            if (!$user->save()) {
                throw new Exception("User couldn't be  saved");
            };
            $user->afterSignup([
                'firstname' => $this->firstName,
                'lastname' => $this->lastName,
                'locale' => $this->language,
            ]);

            return $user;
        }

        return null;
    }

    /**
     * @return bool
     */
    public function shouldBeActivated()
    {
        /** @var Module $userModule */
        $userModule = Yii::$app->getModule('user');
        if (!$userModule) {
            return false;
        } elseif ($userModule->shouldBeActivated) {
            return true;
        } else {
            return false;
        }
    }
}
