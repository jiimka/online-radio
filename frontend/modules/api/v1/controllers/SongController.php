<?php

namespace frontend\modules\api\v1\controllers;

use Carbon\Carbon;
use common\models\Song;
use common\models\SongBroadcast;
use DateInterval;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\{Controller, NotFoundHttpException, Response};

class SongController extends Controller
{
    const SONGS_INTERVAL = 5; // Minutes - for generating test songs order
    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex()
    {
        Yii::$app->response->format = Response::FORMAT_XML;
        Yii::$app->response->formatters = [
            Response::FORMAT_XML => [
                'class' => 'yii\web\XmlResponseFormatter',
                'encoding' => 'UTF-8',
                'rootTag' => 'stream',
            ],
        ];

        $now = Carbon::now();
        $response = $this->formattedResponse($now);

        return $response;
    }

    /**
     * @param \Carbon\Carbon $dateTime
     *
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    protected function formattedResponse(Carbon $dateTime) {
        $startOfDay = Carbon::today()->startOfDay();
        $songsCount = Song::find()->count();
        $offset = intval(floor(($dateTime->diffInMinutes($startOfDay)) / self::SONGS_INTERVAL));
        $rotation = 0;
        while ($offset > $songsCount) {
            $offset = $offset - $songsCount;
            $rotation++;
        }

        $song = Song::find()->offset($offset)->orderBy(['title' => SORT_ASC])->one() ?: Song::find()->one();
        if (!($song instanceof Song)) {
            throw new NotFoundHttpException();
        }

        $startOfDayDateString = $startOfDay->toDateString();
        $songBroadcast = SongBroadcast::find()->where(['song_id' => $song->id])->andWhere(['date' => $startOfDayDateString])->one()
            ?: new SongBroadcast(['broadcast' => 0, 'date' => $startOfDayDateString, 'song_id' => $song->id]);
        $songBroadcast->broadcast++;
        $songBroadcast->save();

        $songStartsAt = clone $startOfDay;
        $songStartsAt->addMinutes(($offset + $songsCount * $rotation ) * self::SONGS_INTERVAL);
        $songEndsAt = clone $songStartsAt;
        list($hours, $minutes, $seconds) = sscanf($song->duration, '%d:%d:%d');
        $songDuration = new DateInterval(sprintf('PT%dH%dM%dS', $hours, $minutes, $seconds));
        $songDurationSeconds = $songDuration->i * 60 + $songDuration->s;
        $songEndsAt->addSeconds(($songDurationSeconds < self::SONGS_INTERVAL * 60) ? $songDurationSeconds : self::SONGS_INTERVAL * 60);
        $songEndsIn = $dateTime->diff($songEndsAt);
        $result = [
            'title' => $song->artist->name . ' - ' . $song->title,
            'album' => $song->album->title ?? '',
            'genre' => implode(', ', ArrayHelper::map($song->genres, 'title', 'title')),
            'duration' => $songDuration->format('%I:%S'),
            'next' => $songEndsIn->format('%I:%S'),
        ];

        return $result;
    }
}
