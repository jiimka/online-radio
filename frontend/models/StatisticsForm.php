<?php

namespace frontend\models;

use common\models\{Artist,
    Song,
    SongBroadcast,
    SongGenre};
use kartik\daterange\DateRangeBehavior;
use Yii;
use yii\base\Model;

/**
 * StatisticsForm is the model behind the contact form.
 */
class StatisticsForm extends Model
{
    const TYPE_MOST_POPULAR_ARTIST = 1;
    const TYPE_MOST_POPULAR_SONG = 2;
    const TYPE_LONGEST_SONG = 3;
    const TYPE_SHORTEST_SONG = 4;
    const TYPE_MOST_POPULAR_GENRE = 5;
    const TYPE_LARGEST_GENRE = 6;

    /** @var string */
    public $filterDateRange;
    /** @var string */
    public $filterDateMax;
    /** @var string */
    public $filterDateMin;
    /** @var int */
    public $type;
    /** @var string */
    public $result;

    public function behaviors()
    {
        return [
            [
                'class' => DateRangeBehavior::class,
                'attribute' => 'filterDateRange',
                'dateStartAttribute' => 'filterDateMin',
                'dateEndAttribute' => 'filterDateMax',
                'dateEndFormat' => false,
                'dateStartFormat' => false,
            ]
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['filterDateRange'], 'match', 'pattern' => '/^.+\s\-\s.+$/'],
            [['filterDateMax', 'filterDateMin'], 'safe'],
        ];
    }

    public static function types(): array {
        return [
            self::TYPE_MOST_POPULAR_ARTIST => Yii::t('frontend', 'Most popular artist'),
            self::TYPE_MOST_POPULAR_SONG => Yii::t('frontend', 'Most popular song'),
            self::TYPE_LONGEST_SONG => Yii::t('frontend', 'Longest song'),
            self::TYPE_SHORTEST_SONG => Yii::t('frontend', 'Shortest song'),
            self::TYPE_MOST_POPULAR_GENRE => Yii::t('frontend', 'Most popular genre'),
            self::TYPE_LARGEST_GENRE => Yii::t('frontend', 'Largest genre'),
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'filterDateRange' => Yii::t('backend', 'Date Range'),
            'type' => Yii::t('frontend', 'Type'),
        ];
    }

    public function setAttributes($values, $safeOnly = true) {
        parent::setAttributes($values, $safeOnly);

        $this->setResult();
    }

    public function getResult() {
        return $this->result;
    }

    protected function setResult() {
        switch ($this->type) {
            case self::TYPE_MOST_POPULAR_ARTIST:
                $result = $this->mostPopularArtist();
                break;
            case self::TYPE_MOST_POPULAR_SONG:
                $result = $this->mostPopularSong();
                break;
            case self::TYPE_LONGEST_SONG:
                $result = $this->longestSong();
                break;
            case self::TYPE_SHORTEST_SONG:
                $result = $this->shortestSong();
                break;
            case self::TYPE_MOST_POPULAR_GENRE:
                $result = $this->mostPopularGenre();
                break;
            case self::TYPE_LARGEST_GENRE:
                $result = $this->largestGenre();
                break;
            default:
                $result = null;
                break;
        }

        $this->result = $result;
    }

    protected function mostPopularArtist(): ?string {
        $result = null;
        $artistQuery = Artist::find()
            ->select('artist.*, SUM(broadcast) as totalBroadcast')
            ->innerJoinWith('songs')
            ->leftJoin(SongBroadcast::tableName(), 'song_broadcast.song_id = song.id')
            ->groupBy('artist.id')
            ->orderBy(['totalBroadcast' => SORT_DESC]);
        if ($this->filterDateMax && $this->filterDateMin) {
            $artistQuery->andFilterWhere(['between', 'song_broadcast.date', $this->filterDateMin, $this->filterDateMax]);
        }

        $artist = $artistQuery->one();
        if ($artist instanceof Artist) {
            $result = $artist->name;
        }

        return $result;
    }

    protected function mostPopularSong(): ?string {
        $result = null;
        $songBroadcastQuery = SongBroadcast::find()->orderBy(['broadcast' => SORT_DESC]);
        if ($this->filterDateMax && $this->filterDateMin) {
            $songBroadcastQuery->andFilterWhere(['between', 'song_broadcast.date', $this->filterDateMin, $this->filterDateMax]);
        }

        $songBroadcast = $songBroadcastQuery->one();
        if ($songBroadcast instanceof SongBroadcast) {
            $song = $songBroadcast->song;
            $result = $song->title . ' (' . $song->artist->name .' )';
        }

        return $result;
    }

    protected function longestSong(): ?string {
        return $this->songByLength(SORT_DESC);
    }

    protected function shortestSong(): ?string {
        return $this->songByLength(SORT_ASC);
    }

    protected function mostPopularGenre(): ?string {
        $result = null;
        $songGenreQuery = SongGenre::find()
            ->select('genre_id, SUM(broadcast) as totalBroadcast')
            ->leftJoin(SongBroadcast::tableName(), 'song_broadcast.song_id = song_genre.song_id')
            ->groupBy('genre_id')
            ->orderBy(['totalBroadcast' => SORT_DESC]);
        if ($this->filterDateMax && $this->filterDateMin) {
            $songGenreQuery->andFilterWhere(['between', 'song_broadcast.date', $this->filterDateMin, $this->filterDateMax]);
        }

        $songGenre = $songGenreQuery->one();
        if ($songGenre instanceof SongGenre) {
            $result = $songGenre->genre->title;
        }

        return $result;
    }

    protected function largestGenre(): ?string {
        $result = null;
        $songGenreQuery = SongGenre::find()
            ->select('genre_id, COUNT(*) as c')
            ->leftJoin(SongBroadcast::tableName(), 'song_broadcast.song_id = song_genre.song_id')
            ->groupBy('genre_id')
            ->orderBy(['c' => SORT_DESC]);
        if ($this->filterDateMax && $this->filterDateMin) {
            $songGenreQuery->andFilterWhere(['between', 'song_broadcast.date', $this->filterDateMin, $this->filterDateMax]);
        }

        $songGenre = $songGenreQuery->one();
        if ($songGenre instanceof SongGenre) {
            $result = $songGenre->genre->title;
        }

        return $result;
    }

    protected function songByLength(int $sort) {
        $result = null;
        $songQuery = Song::find()
            ->select('song_broadcast.*, song.*')
            ->leftJoin(SongBroadcast::tableName(), 'song_broadcast.song_id = song.id')
            ->orderBy(['duration' => $sort]);
        if ($this->filterDateMax && $this->filterDateMin) {
            $songQuery->andFilterWhere(['between', 'song_broadcast.date', $this->filterDateMin, $this->filterDateMax]);
        }

        $song = $songQuery->one();
        if ($song instanceof Song) {
            $result = $song->title . ' - ' .  $song->artist->name . ' (' . $song->duration .' )';
        }

        return $result;
    }
}
