<?php

namespace frontend\controllers;

use frontend\models\StatisticsForm;
use Yii;
use yii\web\{Controller, Response};

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale' => [
                'class' => 'common\actions\SetLocaleAction',
                'locales' => array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string|Response
     */
    public function actionStatistics()
    {
        $model = new StatisticsForm();
        $model->load(Yii::$app->request->post());

        return $this->render('statistics/index', [
            'model' => $model
        ]);
    }
}
