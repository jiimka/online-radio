<?php

return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        // Pages
        ['pattern' => 'page/<slug>', 'route' => 'page/view'],

        // API
        ['pattern' => 'api/v1/song/index', 'route' => 'api/v1/song/index'],
    ]
];
