<?php

use frontend\widgets\Ticker;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->name;
?>
<div class="site-index">
    <div class="d-flex flex-row-reverse">
        <?= Html::a(Yii::t('frontend', 'View Statistics'), ['site/statistics'], ['class' => 'btn btn-primary', 'target' => '_blank']) ;?>
    </div>
    <div class="mb-5">
        <?php echo Ticker::widget([]); ?>
    </div>
</div>
