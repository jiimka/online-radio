<?php

use yii\helpers\{Html};
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model \common\models\Song */
/* @var $key integer */
/* @var $index integer */
/* @var $widget ListView */
?>
<div class="card mb-3">
    <div class="card-header">
        <div class="row">
            <div class="col">
                <?php
                $items = [
                    $model->title,
                    $model->album->title,
                    $model->duration,
                    Html::tag('i', '', ['class' => 'fas fa-play',]),
                ];

                echo Html::ul($items, [
                    'class' => 'list-unstyled list-group list-group-horizontal d-flex flex-column flex-sm-row ',
                    'encode' => false,
                    'itemOptions' => ['class' => 'list-group-item px-3 py-1 border-top-0 border-left-0 border-bottom-0 border-sm-0 bg-transparent rounded-0',],
                ]); ?>
            </div>
        </div>
    </div>
</div>
