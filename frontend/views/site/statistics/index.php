<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \frontend\models\StatisticsForm */

$this->title = Yii::t('frontend', 'Statistics');
?>
<div class="site-stats">
    <div class="row">
        <div class="col-md-6 mb-5">
            <?= Html::tag('h2', $this->title); ?>

            <?php echo $this->render('_search', ['model' => $model,]); ?>
        </div>

        <div class="col-md-6 text-center">
            <div class="card mb-3 text-secondary bg-light">
                <div class="card-body">
                    <?= $model->getResult() ? Html::tag('h5', $model->getResult()) : ''; ?>
                </div>
            </div>
        </div>
    </div>
</div>
