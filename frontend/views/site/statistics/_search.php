<?php

use kartik\daterange\DateRangePicker;
use frontend\models\StatisticsForm;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model StatisticsForm */
/* @var $form yii\bootstrap4\ActiveForm */
?>
<div class="task-search">
    <?php $form = ActiveForm::begin([
        'action' => ['site/statistics'],
        'method' => 'post',
    ]); ?>
    <?php
    $cancelDatepickerJs =  <<<JS
function(ev, picker) {
    var input = $("#statisticsform-filterdaterange");
    input.val('');
    input.trigger('change');
    $(this).find("input.range-value").val('');
  }
JS
        ;
    echo $form->field($model, 'filterDateRange', ['options' => ['class' => 'form-group',],])
        ->widget(DateRangePicker::class, [
            'attribute' => 'filterDateRange',
            'startAttribute' => 'filterDateMin',
            'endAttribute' => 'filterDateMax',
            'convertFormat' => true,
            'hideInput' => true,
            'pluginEvents' => [
                'cancel.daterangepicker' => new JsExpression($cancelDatepickerJs),
            ],
            'pluginOptions' => [
                'autoUpdateInput' => false,
                'locale' => [
                    'cancelLabel' => Yii::t('frontend', 'Clear'),
                    'format' => 'Y-m-d',
                ],
                'timePicker' => false,
            ]
        ]);
    ?>
    <?php echo $form->field($model, 'type')->dropDownList(StatisticsForm::types()); ?>
    <?php echo Html::submitButton(Yii::t('frontend', 'Show'), ['class' => 'btn btn-primary']); ?>
    <?php ActiveForm::end(); ?>
</div>
