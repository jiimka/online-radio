<?php

use kartik\icons\FontAwesomeAsset;
use yii\bootstrap4\{Nav, NavBar};

/* @var $this \yii\web\View */
/* @var $content string */

FontAwesomeAsset::register($this);

$isGuest = Yii::$app->user->isGuest;
$this->beginContent('@frontend/views/layouts/_clear.php');
?>
<div class="wrap bg-site">
    <div class="<?= ($isGuest && Yii::$app->controller->id === 'sign-in') ? 'bg-blue-gradient' : ''; ?>" style="">
        <?php
        NavBar::begin([
            'brandLabel' => Yii::$app->name,
            'brandUrl' => Yii::$app->homeUrl,
            'collapseOptions' => [
                'class' => 'navbar-collapse collapse pl-3',
            ],
            'options' => [
                'class' => 'navbar navbar-expand-lg navbar-light bg-white p-0',
            ],
            'togglerOptions' => [
                'class' => 'navbar-toggler my-3',
            ],
        ]); ?>
        <?php echo Nav::widget([
            'options' => ['class' => 'navbar-nav nav-pills ml-auto'],
            'items' => [
                ['label' => Yii::t('frontend', 'Signup'), 'url' => ['/user/sign-in/signup'], 'visible' => $isGuest],
                ['label' => Yii::t('frontend', 'Login'), 'url' => ['/user/sign-in/login'], 'visible' => $isGuest],
                [
                    'label' => Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->getPublicIdentity(),
                    'visible' => !Yii::$app->user->isGuest,
                    'items' => [
                        [
                            'label' => Yii::t('frontend', 'My Account'),
                            'url' => ['/user/default/index'],
                        ],
                        [
                            'label' => Yii::t('frontend', 'Admin Dashboard'),
                            'url' => Yii::getAlias('@backendUrl'),
                            'visible' => Yii::$app->user->can('manager'),
                        ],
                        [
                            'label' => Yii::t('frontend', 'Logout'),
                            'url' => ['/user/sign-in/logout'],
                            'linkOptions' => ['data-method' => 'post'],
                        ]
                    ]
                ],
            ],
        ]); ?>
        <?php NavBar::end(); ?>

        <?php echo $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
    </div>
</footer>
<?php $this->endContent(); ?>
