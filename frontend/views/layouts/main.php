<?php
use yii\bootstrap4\Alert;
use yii\helpers\ArrayHelper;

/* @var $this \yii\web\View */
/* @var $content string */

$this->beginContent('@frontend/views/layouts/base.php');
?>
    <div class="container-fluid mt-5 content-wrapper">
        <?php if (Yii::$app->session->hasFlash('alert')): ?>
            <?php echo Alert::widget([
                'body' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
                'options' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
            ]); ?>
        <?php endif; ?>

        <?php echo $content; ?>
    </div>
<?php $this->endContent(); ?>
