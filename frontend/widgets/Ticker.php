<?php

namespace frontend\widgets;


use yii\base\Widget;

class Ticker extends Widget
{
    public function run()
    {
        return $this->render('ticker/index');
    }
}
