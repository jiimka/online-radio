<?php

/* @var $this yii\web\View */

use yii\helpers\Html; ?>

<div class="client-object-index box box-primary">
    <div class="box-header with-border">
        <h4 class="box-title"><?= Yii::t('frontend', 'Now Playing'); ?></h4>
    </div>
    <div class="box-body">
        <div class="progress" style="height: 20px;">
            <div id="ticker-progress-bar" class="progress-bar" role="progressbar" style="width: 0;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
        </div>

        <div class="card mt-3" id="ticker-song-details">
            <div class="card-header">
                <div class="row">
                    <div class="col">
                        <?php
                        $items = [
                            Html::tag('span', Yii::t('frontend', 'Title')) . ': ' . Html::tag('span', '', ['id' => 'ticker-song-title']),
                            Html::tag('span', Yii::t('frontend', 'Album')) . ': ' . Html::tag('span', '', ['id' => 'ticker-song-album']),
                            Html::tag('span', Yii::t('frontend', 'Genre')) . ': ' . Html::tag('span', '', ['id' => 'ticker-song-genre']),
                            Html::tag('span', Yii::t('frontend', 'Length')) . ': ' . Html::tag('span', '', ['id' => 'ticker-song-length']),
                            Html::tag('span', Yii::t('frontend', 'Time Left')) . ': ' . Html::tag('span', '', ['id' => 'ticker-song-time-left']),
                        ];
                        echo Html::ul($items, [
                            'class' => 'list-unstyled list-group list-group-horizontal d-flex flex-column flex-sm-row ',
                            'encode' => false,
                            'itemOptions' => ['class' => 'list-group-item px-3 py-1 border-top-0 border-left-0 border-bottom-0 border-sm-0 bg-transparent rounded-0',],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$actionUrl = Yii::$app->urlManagerFrontend->createAbsoluteUrl(['/api/v1/song/index']);

$script = <<<JS
$(document).ready(function() {
    let refreshSong = function() {
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
              updateSongDetails(this);
            }
        };
        xhttp.open("GET", "$actionUrl", true);
        xhttp.send();
              
        let updateSongDetails = function updateSongDetails(xml) {
            let i,
            xmlDoc = xml.responseXML,
            x = xmlDoc.getElementsByTagName("stream");
            for (i = 0; i < x.length; i++) {
                document.getElementById("ticker-song-title").innerHTML = x[i].getElementsByTagName("title")[0].childNodes[0].nodeValue;
                document.getElementById("ticker-song-album").innerHTML = x[i].getElementsByTagName("album")[0].childNodes[0].nodeValue;
                document.getElementById("ticker-song-genre").innerHTML = x[i].getElementsByTagName("genre")[0].childNodes[0].nodeValue;
                let duration = x[i].getElementsByTagName("duration")[0].childNodes[0].nodeValue,
                timeLeft = x[i].getElementsByTagName("next")[0].childNodes[0].nodeValue,
                currentProgress = ((timeToSeconds(duration) - timeToSeconds(timeLeft)) / timeToSeconds(duration)) * 100,
                progressBar = document.getElementById("ticker-progress-bar");
                document.getElementById("ticker-song-length").innerHTML = duration;
                document.getElementById("ticker-song-time-left").innerHTML = timeLeft;
                progressBar.style.width = currentProgress + '%';
                progressBar.setAttribute("aria-valuenow", currentProgress.toString());
            }
        };

        return false;
    };
    
    function timeToSeconds(time) {
      let b = time.split(':');
      
      return parseInt(b[0]*60) + parseInt(b[1]);
    }
    
    setInterval(function() { refreshSong(); }, 1000);
});
JS;

$this->registerJs($script);
?>
