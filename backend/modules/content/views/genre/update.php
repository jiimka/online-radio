<?php

/* @var $this yii\web\View */
/* @var $model common\models\Genre */

$this->title = Yii::t('backend', 'Update Genre') . ': ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Genres'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="genre-update">
    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
