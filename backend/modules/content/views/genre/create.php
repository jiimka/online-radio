<?php

/* @var $this yii\web\View */
/* @var $model common\models\Genre */

$this->title = Yii::t('backend', 'Create Genre');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Genres'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="genre-create">
    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
