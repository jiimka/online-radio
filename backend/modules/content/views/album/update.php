<?php

/* @var $this yii\web\View */
/* @var $artists common\models\Artist[]|array */
/* @var $model common\models\Album */

$this->title = Yii::t('backend', 'Update Album') . ': ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Albums'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="album-update">
    <?php echo $this->render('_form', [
        'artists' => $artists,
        'model' => $model,
    ]) ?>
</div>
