<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $artists common\models\Artist[]|array */
/* @var $model common\models\Album */
/* @var $form yii\bootstrap\ActiveForm */
?>
<div class="album-form">
    <?php $form = ActiveForm::begin(); ?>
    <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'year')->dropDownList(range(1950, date('Y'))) ?>

    <?php echo $form->field($model, 'artist_id')->dropDownList(ArrayHelper::map($artists, 'id', 'name')) ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
