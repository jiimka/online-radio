<?php

/* @var $this yii\web\View */
/* @var $artists common\models\Artist[]|array */
/* @var $model common\models\Album */

$this->title = Yii::t('backend', 'Create Album');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Albums'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="album-create">
    <?php echo $this->render('_form', [
        'artists' => $artists,
        'model' => $model,
    ]) ?>
</div>
