<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\content\models\search\AlbumSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Albums');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="album-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?php echo Html::a(Yii::t('backend', 'Create Album'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'title',
            'year',
            'artist_id',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
