<?php

/* @var $this yii\web\View */
/* @var $model common\models\Artist */

$this->title = Yii::t('backend', 'Create Artist');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Artists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="artist-create">
    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
