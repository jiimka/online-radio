<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\content\models\search\SongSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Songs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="song-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?php echo Html::a(Yii::t('backend', 'Create Song'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'title',
            'artist_id',
            'album_id',
            'duration',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
