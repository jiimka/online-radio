<?php

/* @var $this yii\web\View */
/* @var $albums common\models\Album[]|array */
/* @var $artists common\models\Artist[]|array */
/* @var $model common\models\Song */

$this->title = Yii::t('backend', 'Create Song');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Songs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="song-create">
    <?php echo $this->render('_form', [
        'albums' => $albums,
        'artists' => $artists,
        'model' => $model,
    ]) ?>
</div>
