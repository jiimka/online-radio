<?php

/* @var $this yii\web\View */
/* @var $albums common\models\Album[]|array */
/* @var $artists common\models\Artist[]|array */
/* @var $model common\models\Song */

$this->title = Yii::t('backend', 'Update Song') . ': ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Songs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="song-update">
    <?php echo $this->render('_form', [
        'albums' => $albums,
        'artists' => $artists,
        'model' => $model,
    ]) ?>
</div>
