<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\{ArrayHelper, Html};

/* @var $this yii\web\View */
/* @var $albums common\models\Album[]|array */
/* @var $artists common\models\Artist[]|array */
/* @var $model common\models\Song */
/* @var $form yii\bootstrap\ActiveForm */
?>
<div class="song-form">
    <?php $form = ActiveForm::begin(); ?>
    <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'artist_id')->dropDownList(ArrayHelper::map($artists, 'id', 'name')) ?>

    <?php echo $form->field($model, 'album_id')->dropDownList(ArrayHelper::map($albums, 'id', 'title', 'artist.name')) ?>

    <?php echo $form->field($model, 'duration')->input('time', ['step' => 1]) ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
