<?php

namespace backend\modules\content\models\search;


use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Song;

/**
 * SongSearch represents the model behind the search form about `common\models\Song`.
 */
class SongSearch extends Song
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'artist_id', 'album_id'], 'integer'],
            [['title', 'duration'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Song::find()
            ->joinWith(['album', 'artist', 'genres'])
            ->limit(100);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'artist_id' => $this->artist_id,
            'album_id' => $this->album_id,
            'duration' => $this->duration,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
