# Console
## AppController
``php console/yii app/setup`` 

## RbacMigrateController
Provides migrate functionality for RBAC.

``php console/yii rbac-migrate/create init_roles``

``php console/yii rbac-migrate/up``

``php console/yii rbac-migrate/down all``
